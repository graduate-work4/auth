import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { join } from "path";
import { INestMicroservice, ValidationPipe } from '@nestjs/common';
import { protobufPackage } from './auth/proto/auth.pb';
import { Transport } from '@nestjs/microservices';

async function bootstrap() {
  const app: INestMicroservice = await NestFactory.createMicroservice(AppModule,{
    transport: Transport.GRPC,
    options:{
      url: '0.0.0.0:50052',
      package: protobufPackage,
      protoPath: join('node_modules/gprc-node-proto/proto/auth.proto'),
    }

  })
  app.useGlobalPipes(new ValidationPipe({ whitelist: true, transform: true }))

  await app.listen()
}
bootstrap();
