import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [TypeOrmModule.forRoot({
    type: 'postgres',
    host: 'localhost',
    port: 5432,
    database: 'test',
    username: 'postgres',
    password: '12345',
    entities: [__dirname + "/**/*.entity{.ts,.js}"],
    synchronize: true,
  }),AuthModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
