import { HttpStatus, Inject, Injectable, OnModuleInit } from '@nestjs/common';
import { CreateUserResponse, USER_SERVICE_NAME, UserServiceClient } from './proto/user.pb';
import { ClientGrpc } from '@nestjs/microservices';
import { InjectRepository } from '@nestjs/typeorm';
import { Auth } from './auth.entity';
import {
  AuthRequest,
  AuthResponse,
  LogoutRequest,
  LogoutResponse,
  RefreshResponse,
} from './proto/auth.pb';
import { firstValueFrom, Observable } from 'rxjs';
import { AuthDto } from './auth.dto';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { Repository } from 'typeorm';

@Injectable()
export class AuthService implements OnModuleInit {
  constructor(@Inject(USER_SERVICE_NAME) private readonly client: ClientGrpc, @InjectRepository(Auth) private authRepository: Repository<Auth>, private jwtService: JwtService) {
  }

  private userSvc: UserServiceClient;

  onModuleInit(): void {
    this.userSvc = this.client.getService<UserServiceClient>(USER_SERVICE_NAME);
  }

  async login(request: AuthDto): Promise<AuthResponse | Observable<AuthResponse>> {
    const data = await firstValueFrom(this.userSvc.getUserForLogin({phone:request.phone}))
    if (data.status === HttpStatus.NOT_FOUND){
      return {status:data.status,error:['не коректный телефон или пороль'],refreshToken:'',accessToken:''}
    }
    if (data.error?.length){
      return {status:data.status,error:data.error,refreshToken:'',accessToken:''}
    }
    const campfirePassword = await bcrypt.compare(request.password, data.password);
    if (!campfirePassword){
      return {status:422,error:['не коректный телефон или пороль'],refreshToken:'',accessToken:''}
    }
    const tokens = this.generationToken(+data.id);
    await this.saveToken(+data.id,tokens.refresh_token)
    return {
      accessToken: tokens.access_token,
      refreshToken: tokens.refresh_token,
      error: null,
      status: HttpStatus.OK,
    };
  }

  async logout(request: LogoutRequest): Promise<LogoutResponse | Observable<LogoutResponse>> {
    await this.authRepository.delete({ token: request.refreshToken });
    return {};
  }

  async registration(request: AuthRequest): Promise<AuthResponse | Observable<AuthResponse>> {
    request.password = await bcrypt.hash(request.password, 5);
    const data: CreateUserResponse = await firstValueFrom(this.userSvc.createUser(request));
    if (data.error?.length) {
      return { accessToken: '', refreshToken: '', error: data.error, status: data.status };
    }
    const tokens = await this.generationToken(data.id);
    await this.authRepository.save({ userId: +data.id, token: tokens.refresh_token });
    return {
      accessToken: tokens.access_token,
      refreshToken: tokens.refresh_token,
      error: null,
      status: HttpStatus.CREATED,
    };
  }

  private async saveToken(userId: number, token: string) {
    const candidate = await this.authRepository.findOne({ where: { userId } });
    if (candidate) {
      return await this.authRepository.update({ id: candidate.id }, { token });
    }
    return await this.authRepository.save({ token });
  }

  private generationToken(id: number) {
    const payload = { id };
    return {
      access_token: this.jwtService.sign(payload),
      refresh_token: this.jwtService.sign(payload, { secret: 'refresh', expiresIn: '30d' }),
    };
  }

  private async findRefreshToken(token: string) {
    return await this.authRepository.findOne({ where: { token } });
  }

  private verifyRefreshToken(refresh_token: string): { id: number } {
    return this.jwtService.verify(refresh_token, { secret: 'refresh' });
  }

  async refresh(refresh_token): Promise<RefreshResponse> {
    if (!refresh_token) {
      return { error: ['вы не загерестрированы'], accessToken: '', refreshToken: '', status: 400 };
    }
    const verifyToken = this.verifyRefreshToken(refresh_token);
    const refreshTokenFromDB = await this.findRefreshToken(refresh_token);
    if (!verifyToken || !refreshTokenFromDB) {
      return { error: ['вы не загерестрированы'], accessToken: '', refreshToken: '', status: 400 };
    }
    const user = await this.findOneUserById(verifyToken.id);
    if (user.error?.length) {
      return { error: user.error, accessToken: '', refreshToken: '', status: user.status };
    }
    const tokens = this.generationToken(+user.userId);
    await this.saveToken(+user.userId, tokens.refresh_token);
    return { error: null, accessToken: tokens.access_token, refreshToken: tokens.refresh_token, status: HttpStatus.OK };
  }

  async findOneUserById(id: number) {
    return await firstValueFrom(this.userSvc.getUserById({ userId: id, error: null, status: null }));
  }
}