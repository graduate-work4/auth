import { Controller } from '@nestjs/common';
import {
  AUTH_SERVICE_NAME,
  AuthRequest,
  AuthResponse,
  LogoutRequest,
  LogoutResponse,
  RefreshRequest, RefreshResponse,
} from './proto/auth.pb';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { GrpcMethod } from '@nestjs/microservices';
import { AuthDto } from './auth.dto';

@Controller()
export class AuthController {
  constructor(private authService: AuthService) {}

  @GrpcMethod(AUTH_SERVICE_NAME, 'login')
   login(request: AuthDto): Promise<AuthResponse | Observable<AuthResponse>> {
    return this.authService.login(request);
  }

  @GrpcMethod(AUTH_SERVICE_NAME, 'logout')
  logout(request: LogoutRequest): Promise<LogoutResponse> | Observable<LogoutResponse> | LogoutResponse {
    return this.authService.logout(request);
  }

  @GrpcMethod(AUTH_SERVICE_NAME, 'refresh')
  refresh(request: RefreshRequest): Promise<RefreshResponse | Observable<RefreshResponse>> {
    return this.authService.refresh(request.refreshToken);
  }

  @GrpcMethod(AUTH_SERVICE_NAME, 'registration')
  registration(request: AuthRequest): Promise<AuthResponse | Observable<AuthResponse>> {
    return this.authService.registration(request);
  }
}