import { AuthRequest } from './proto/auth.pb';
import { IsNotEmpty } from 'class-validator';

export class AuthDto implements AuthRequest{
  @IsNotEmpty({message:'пороль не должен быть пустым'})
  password: string;
  @IsNotEmpty({message:'телефон не должен быть пустым'})
  phone: string;
}