/* eslint-disable */
import { GrpcMethod, GrpcStreamMethod } from "@nestjs/microservices";
import { Observable } from "rxjs";

export const protobufPackage = "auth";

export interface AuthResponse {
  accessToken: string;
  refreshToken: string;
  error: string[];
  status: number;
}

export interface AuthRequest {
  phone: string;
  password: string;
}

export interface LogoutRequest {
  refreshToken: string;
}

export interface LogoutResponse {
}

export interface RefreshRequest {
  refreshToken: string;
}

export interface RefreshResponse {
  accessToken: string;
  refreshToken: string;
  error: string[];
  status: number;
}

export const AUTH_PACKAGE_NAME = "auth";

export interface AuthServiceClient {
  login(request: AuthRequest): Observable<AuthResponse>;

  registration(request: AuthRequest): Observable<AuthResponse>;

  logout(request: LogoutRequest): Observable<LogoutResponse>;

  refresh(request: RefreshRequest): Observable<RefreshResponse>;
}

export interface AuthServiceController {
  login(request: AuthRequest): Promise<AuthResponse> | Observable<AuthResponse> | AuthResponse;

  registration(request: AuthRequest): Promise<AuthResponse> | Observable<AuthResponse> | AuthResponse;

  logout(request: LogoutRequest): Promise<LogoutResponse> | Observable<LogoutResponse> | LogoutResponse;

  refresh(request: RefreshRequest): Promise<RefreshResponse> | Observable<RefreshResponse> | RefreshResponse;
}

export function AuthServiceControllerMethods() {
  return function (constructor: Function) {
    const grpcMethods: string[] = ["login", "registration", "logout", "refresh"];
    for (const method of grpcMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(constructor.prototype, method);
      GrpcMethod("AuthService", method)(constructor.prototype[method], method, descriptor);
    }
    const grpcStreamMethods: string[] = [];
    for (const method of grpcStreamMethods) {
      const descriptor: any = Reflect.getOwnPropertyDescriptor(constructor.prototype, method);
      GrpcStreamMethod("AuthService", method)(constructor.prototype[method], method, descriptor);
    }
  };
}

export const AUTH_SERVICE_NAME = "AuthService";
