import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { USER_PACKAGE_NAME, USER_SERVICE_NAME } from './proto/user.pb';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Auth } from './auth.entity';
import { JwtModule } from '@nestjs/jwt';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';

@Module({
  imports: [ClientsModule.register([{
    name: USER_SERVICE_NAME,
    transport: Transport.GRPC,
    options: {
      url: '0.0.0.0:50053',
      package: USER_PACKAGE_NAME,
      protoPath: 'node_modules/gprc-node-proto/proto/user.proto',
    },
  }]), JwtModule.register({
    secret: 'hello world',
    signOptions: { expiresIn: '180m' },
  }), TypeOrmModule.forFeature([Auth])],
  exports: [],
  controllers: [AuthController], providers: [AuthService],
})
export class AuthModule {
}